package de.quandoo.recruitment.registry.model;

public class CuisinesTopEntry implements Comparable<CuisinesTopEntry> {

    private final Cuisine cuisine;
    private final int score;

    public CuisinesTopEntry(Cuisine cuisine, int score) {
        this.cuisine = cuisine;
        this.score = score;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    public int getScore() {
        return score;
    }

    @Override
    public int compareTo(CuisinesTopEntry other) {
        int result = -Integer.compare(score, other.score);
        if (result != 0) {
            return result;
        } else {
            return -cuisine.compareTo(other.cuisine);
        }
    }
}
