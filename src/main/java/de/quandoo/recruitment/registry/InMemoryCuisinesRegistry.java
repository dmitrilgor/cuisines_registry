package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisinesTopEntry;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, Set<Customer>> customersPerCuisines = new HashMap<>();
    private Map<Customer, Set<Cuisine>> cuisinesPerCustomers = new HashMap<>();

    @Override
    public synchronized void register(final Customer customer, final Cuisine cuisine) {
        getCustomers(cuisine).add(customer);
        getCuisine(customer).add(cuisine);
    }

    @Override
    public synchronized List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return new ArrayList<>(getCustomers(cuisine));
    }

    @Override
    public synchronized List<Cuisine> customerCuisines(final Customer customer) {
        return new ArrayList<>(getCuisine(customer));
    }

    @Override
    public synchronized List<Cuisine> topCuisines(final int n) {
        if (n < 0) {
            throw new IllegalStateException("Rating size cannot be bounded above by negative");
        }

        if (n == 0) {
            return Collections.emptyList();
        }

        Set<CuisinesTopEntry> top = new TreeSet<>();

        for (Map.Entry<Cuisine, Set<Customer>> entry : customersPerCuisines.entrySet()) {
            top.add(new CuisinesTopEntry(entry.getKey(), entry.getValue().size()));
        }

        return top.stream()
                .limit(n)
                .map(CuisinesTopEntry::getCuisine)
                .collect(Collectors.toList());
    }

    private Set<Customer> getCustomers(Cuisine cuisine) {
        return cuisine == null ? Collections.emptySet() : customersPerCuisines.computeIfAbsent(cuisine, key -> new HashSet<>());
    }

    private Set<Cuisine> getCuisine(Customer customer) {
        return customer == null ? Collections.emptySet() : cuisinesPerCustomers.computeIfAbsent(customer, key -> new HashSet<>());
    }
}
