package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static java.util.Collections.*;

public class InMemoryCuisinesRegistryTest {

    public static final Cuisine ITALIAN = new Cuisine("italian");
    public static final Cuisine FRENCH = new Cuisine("french");
    public static final Cuisine GERMAN = new Cuisine("german");
    public static final Cuisine KOREAN = new Cuisine("korean");
    public static final Cuisine CHINESE = new Cuisine("chinese");

    public static final Customer CUSTOMER_1 = new Customer("1");
    public static final Customer CUSTOMER_2 = new Customer("2");
    public static final Customer CUSTOMER_3 = new Customer("3");
    public static final Customer CUSTOMER_4 = new Customer("4");

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void testGetCustomersByCuisine() {
        cuisinesRegistry.register(CUSTOMER_1, FRENCH);
        cuisinesRegistry.register(CUSTOMER_2, GERMAN);
        cuisinesRegistry.register(CUSTOMER_3, ITALIAN);

        assertCustomersForCuisine(FRENCH, singleton(CUSTOMER_1));
        assertCustomersForCuisine(GERMAN, singleton(CUSTOMER_2));
        assertCustomersForCuisine(ITALIAN, singleton(CUSTOMER_3));
        assertCustomersForCuisine(KOREAN, emptySet());
        assertCustomersForCuisine(CHINESE, emptySet());


        cuisinesRegistry.register(CUSTOMER_1, ITALIAN);
        cuisinesRegistry.register(CUSTOMER_1, GERMAN);
        cuisinesRegistry.register(CUSTOMER_2, ITALIAN);
        cuisinesRegistry.register(CUSTOMER_4, KOREAN);

        assertCustomersForCuisine(FRENCH, singleton(CUSTOMER_1));
        assertCustomersForCuisine(GERMAN, newHashSet(CUSTOMER_1, CUSTOMER_2));
        assertCustomersForCuisine(ITALIAN, newHashSet(CUSTOMER_1, CUSTOMER_2, CUSTOMER_3));
        assertCustomersForCuisine(KOREAN, singleton(CUSTOMER_4));
        assertCustomersForCuisine(CHINESE, emptySet());
    }

    @Test
    public void testGetCustomersForNullCuisine() {
        assertCustomersForCuisine(null, emptySet());
    }

    private void assertCustomersForCuisine(Cuisine cuisine, Set<Customer> expectedCustomers) {
        Assert.assertEquals(expectedCustomers, new HashSet<>(cuisinesRegistry.cuisineCustomers(cuisine)));
    }

    @Test
    public void testGetCuisinesByCustomer() {
        cuisinesRegistry.register(CUSTOMER_1, FRENCH);
        cuisinesRegistry.register(CUSTOMER_2, GERMAN);
        cuisinesRegistry.register(CUSTOMER_3, ITALIAN);

        assertCuisinesForCustomer(CUSTOMER_1, singleton(FRENCH));
        assertCuisinesForCustomer(CUSTOMER_2, newHashSet(GERMAN));
        assertCuisinesForCustomer(CUSTOMER_3, singleton(ITALIAN));
        assertCuisinesForCustomer(CUSTOMER_4, emptySet());


        cuisinesRegistry.register(CUSTOMER_2, FRENCH);
        cuisinesRegistry.register(CUSTOMER_2, ITALIAN);
        cuisinesRegistry.register(CUSTOMER_4, KOREAN);

        assertCuisinesForCustomer(CUSTOMER_1, singleton(FRENCH));
        assertCuisinesForCustomer(CUSTOMER_2, newHashSet(FRENCH, GERMAN, ITALIAN));
        assertCuisinesForCustomer(CUSTOMER_3, singleton(ITALIAN));
        assertCuisinesForCustomer(CUSTOMER_4, singleton(KOREAN));
    }

    @Test
    public void testGetCuisinesForNullCustomer() {
        assertCuisinesForCustomer(null, emptySet());
    }

    private void assertCuisinesForCustomer(Customer customer, Set<Cuisine> expectedCuisines) {
        Assert.assertEquals(expectedCuisines, new HashSet<>(cuisinesRegistry.customerCuisines(customer)));
    }

    @Test
    public void testTopCuisinesEmptyWhenNoCustomers() {
        Assert.assertTrue(cuisinesRegistry.topCuisines(0).isEmpty());
        Assert.assertTrue(cuisinesRegistry.topCuisines(1).isEmpty());
        Assert.assertTrue(cuisinesRegistry.topCuisines(100).isEmpty());
    }

    @Test
    public void testTopCuisines() {
        cuisinesRegistry.register(CUSTOMER_1, FRENCH);
        cuisinesRegistry.register(CUSTOMER_2, FRENCH);
        cuisinesRegistry.register(CUSTOMER_3, FRENCH);

        cuisinesRegistry.register(CUSTOMER_2, GERMAN);

        cuisinesRegistry.register(CUSTOMER_2, ITALIAN);
        cuisinesRegistry.register(CUSTOMER_3, ITALIAN);

        cuisinesRegistry.register(CUSTOMER_4, KOREAN);


        Assert.assertTrue(cuisinesRegistry.topCuisines(0).isEmpty());
        Assert.assertEquals(singletonList(FRENCH), cuisinesRegistry.topCuisines(1));
        Assert.assertEquals(asList(FRENCH, ITALIAN), cuisinesRegistry.topCuisines(2));
        Assert.assertEquals(asList(FRENCH, ITALIAN, GERMAN, KOREAN), cuisinesRegistry.topCuisines(4));
        Assert.assertEquals(asList(FRENCH, ITALIAN, GERMAN, KOREAN), cuisinesRegistry.topCuisines(6));
        Assert.assertEquals(asList(FRENCH, ITALIAN, GERMAN, KOREAN), cuisinesRegistry.topCuisines(100));
        Assert.assertEquals(asList(FRENCH, ITALIAN, GERMAN, KOREAN), cuisinesRegistry.topCuisines(Integer.MAX_VALUE));
    }

    @Test
    public void testTopCuisinesByAlphabeticalOrderWhenAllEquallyPopular() {
        cuisinesRegistry.register(CUSTOMER_1, CHINESE);
        cuisinesRegistry.register(CUSTOMER_1, FRENCH);
        cuisinesRegistry.register(CUSTOMER_2, GERMAN);
        cuisinesRegistry.register(CUSTOMER_3, ITALIAN);
        cuisinesRegistry.register(CUSTOMER_4, KOREAN);

        Assert.assertEquals(asList(CHINESE, FRENCH, GERMAN, ITALIAN, KOREAN), cuisinesRegistry.topCuisines(5));
    }

    @Test
    public void testTopCuisinesWithNegativeRatingLimit() {
        assertIllegalLimit(-1);

        assertIllegalLimit(-31415926);

        assertIllegalLimit(Integer.MIN_VALUE);
    }

    private void assertIllegalLimit(int limit) {
        IllegalStateException actualException = null;
        try {
            cuisinesRegistry.topCuisines(limit);
        } catch (IllegalStateException e) {
            actualException = e;
        }
        Assert.assertNotNull("Should throw IllegalStateException", actualException);
        Assert.assertEquals("Rating size cannot be bounded above by negative", actualException.getMessage());
    }
}